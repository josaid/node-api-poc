// Arquivo: app.js
var express = require("express"),
  app = express();

app.get("/hello-world", function (req, res) {
  res.send("Hello World!");
});

app.get("/users", function (req, res) {
  const users = [
    {
      id: 1,
      username: "bill.gates",
      firstName: "Bill",
      lastName: "Gates",
    },
    {
      id: 2,
      username: "steve.jobs",
      firstName: "Steve",
      lastName: "Jobs",
    },
    {
      id: 3,
      username: "mark.zuckerberg",
      firstName: "Mark",
      lastName: "Zuckerberg",
    },
    {
      id: 4,
      username: "evan.spiegel",
      firstName: "Evan",
      lastName: "Spiegel",
    },
    {
      id: 5,
      username: "jack.dorsey",
      firstName: "Jack",
      lastName: "Dorsey",
    },
  ];

  res.send(JSON.stringify({ statusCode: 200, data: users }));
});

app.listen(3000);
